let companyList = {
  status: 200,
  message: 'success',
  data: {
    total: 100,
    'rows|10': [{
      'id|+1': 1,
      name: '@cname'
    }]
  }
};
export default {
    'get|/api/companyList': companyList
}