const Mock = require('mockjs');
let Random = Mock.Random;
let userList = {
  status: 200,
  message: 'success',
  data: {
    total: 100,
    'rows|10': [{
      'id|+1': 1,
      name: '@cname',
      'company|1': Random.string(20),
      'phone|1': Random.integer(15562623001, 15562623999),
      sign:Random.boolean(),
      createtime: Random.date('y-MM-dd')
    }]
  }
};
let login = {
  "result":
    {
      "token":"eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MDU3NTE4NjMxNDcsInVzZXJpZCI6IjEifQ.fjka30pCDMH0fIgMz_A8Fti2IPviOGeuiemQmCUsONM"
    },
    "_success":true,
    "_message":"",
    "code":"200"
};

export default {
    'get|/api/userList': userList
    // 'get|/api/authen/getToken':login
}