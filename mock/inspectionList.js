const Mock = require('mockjs');
let Random = Mock.Random;
let inspectionList = {
  status: 200,
  message: 'success',
  data: {
    total: 100,
    'rows|10': [{
      'id|+1': 1,
      xjrwmc: '@cname',
      'xjdw|1': Random.string(10),
      'xjzq|1': Random.string(10),
      xjkssj: Random.date('y-MM-dd'),
      'xjryid|+1': 1
    }]
  }
};
export default {
    'get|/api/inspectionList': inspectionList,
    'get|/api/searchinspection': inspectionList,
    'post|/api/inspection': inspectionList
}