const Mock = require('mockjs');
let Random = Mock.Random;
let reportList = {
  status: 200,
  message: 'success',
  data: {
    total: 100,
    'rows|10': [{
      'id|+1': 1,
      vendername: '@cname',
      'venderdomain|1': Random.string(20),
      'venderstatus|1': Random.string(10),
      startTime: Random.date('y-MM-dd'),
      user: '@cname',
      image: Random.string(20)
    }]
  }
};
export default {
    'get|/api/reportList': reportList,
    'post|/api/report': reportList
}