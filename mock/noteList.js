const Mock = require('mockjs');
let Random = Mock.Random;
let noteList = {
  status: 200,
  message: 'success',
  data: {
    total: 100,
    'rows|10': [{
      'id|+1': 1,
      name: '@cname',
      'text': Random.string(2000),
      createtime: Random.date('y-MM-dd')
    }]
  }
};
export default {
    'get|/api/noteList': noteList,
    'post|/api/note':noteList
}