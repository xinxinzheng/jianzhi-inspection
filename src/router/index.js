import Vue from 'vue'
import Router from 'vue-router';

Vue.use(Router);

const Login = resolve => {
  require.ensure(['../view/login'], () => {
    resolve(require('../view/login'));
  });
}

const LoginRoute = [{
  path: '/login',
  name: 'login',
  component: Login
}]

const Main = resolve => {
  require.ensure(['../view/main'], () => {
    resolve(require('../view/main'));
  });
}

const Inspection = resolve => {
  require.ensure(['../view/inspection'], () => {
    resolve(require('../view/inspection'));
  });
}

const HistoryIns = resolve => {
  require.ensure(['../view/historyIns'], () => {
    resolve(require('../view/historyIns'));
  });
}

const ReportList = resolve => {
  require.ensure(['../view/reportList'], () => {
    resolve(require('../view/reportList'));
  });
}

const UserList = resolve => {
  require.ensure(['../view/userList'], () => {
    resolve(require('../view/userList'));
  });
}

const NoteList = resolve => {
  require.ensure(['../view/noteList'], () => {
    resolve(require('../view/noteList'));
  });
}

const InspectionRoute = [{
  path: '/',
  name: 'main',
  redirect: 'inspection',
  component: Main,
  children:[{
    path: 'inspection',
    name: 'inspection',
    component: Inspection
  },{
    path: 'historyIns',
    name: 'historyIns',
    component: HistoryIns
  },{
    path: 'reportList',
    name: 'reportList',
    component: ReportList
  },{
    path: 'userList',
    name: 'userList',
    component: UserList
  },{
    path: 'noteList',
    name: 'noteList',
    component: NoteList
  }]
}]

const router = new Router({
  routes: [
    ...LoginRoute,
    ...InspectionRoute
  ]
})

export default router