import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '../public/index.css';
import router from './router';
import App from './App';
import axios from 'axios';
import Cookies from 'js-cookie'
const jsonHeaders = 'application/json;charset=UTF-8'
const urlHeaders = 'application/x-www-form-urlencoded'
require('../mock');

Vue.use(ElementUI)

Vue.config.productionTip = false

axios.interceptors.request.use(config => {
  config.headers['Content-Type'] = config.data && config.data['IS_FORMDATA'] ? jsonHeaders : urlHeaders
  config.headers['Authorization'] = Cookies.get('token')
  return config
}, error => {
  LoadingBar.error(error)
  Message.error({
    message: '加载超时'
  })
  return Promise.reject(error)
});

axios.interceptors.response.use((res) => {
  let { data } = res
  if (data._success === false) {
    // 后端服务在个别情况下回报201，待确认
    if (parseInt(data.code) === 401) {
      Cookies.remove(TOKEN_KEY)
      window.location.href = window.location.pathname + '#/login'
    } else {
      ElementUI.Message({message: data._message, type: 'error'});
    }
    return Promise.reject(new Error({code: data.code, error: data._message}))
  }
  return res
}, (error) => {
  ElementUI.Message({message: '服务器内部错误', type: 'error'});
  // 对响应错误做点什么
  return Promise.reject(error)
})

Vue.prototype.$http = Vue.$http = axios;

new Vue({
  render: h => h(App),
  router,
  components:{App}
}).$mount("#app")
